function toggleSidebar(id) {
    const elem = document.getElementById(id);
    const classes = elem.className.split(' ');
    const collapsed = classes.indexOf('collapsed') !== -1;
    const padding = {};
    if (collapsed) {
        // Remove the 'collapsed' class from the class list of the element, this sets it back to the expanded state.
        classes.splice(classes.indexOf('collapsed'), 1);
        padding[id] = 300; // In px, matches the width of the sidebars set in .sidebar CSS class
    } else {
        padding[id] = 0;
        // Add the 'collapsed' class to the class list of the element
        classes.push('collapsed');
    }
    // Update the class list on the element
    elem.className = classes.join(' ');
}

function format_info_box(data) {
    console.log('data')
    console.log(data)

    length = data['Length']
    if(data['Length'] == undefined || data['Length'] == null ){
        // Convert data['length'] to km and round to 1 decimal
        length = (data['length'] / 1000).toFixed(1)
    }
    let htmlstr = ''
    htmlstr += `<h3>${data['Lijn']}</h3>`
    htmlstr += `Operator: ${data['Operator']}`
    htmlstr += `<br>Geopend: ${data['From']}`
    htmlstr += `<br>Gesloten: ${data['Until']}`
    htmlstr += `<br>Lengte: ${length} km`
    htmlstr += `<br>Spoorbreedte: ${data['Track Gauge']}`
    htmlstr += `<br>Informatie: ${data['description']}`
    return htmlstr
}
(async () => {  
    let positron_layers = await fetch('data/style/positron_layers.json')
        .then(function (response) {
            return response.json();
        }).catch(function (err) {
            console.log('error: ' + err);
        });
    
    const stylingMap = await fetch('data/style/stylingMap.json')
        .then(function (response) {
            return response.json();
        }).catch(function (err) {
            console.log('error: ' + err);
        });

console.log(positron_layers)
console.debug('Loaded stylingMap')
const map = new maplibregl.Map({
container: 'map', 
style: 'https://basemaps.cartocdn.com/gl/positron-gl-style/style.json',
// style: {
//     'version': 8,
//     'sources': {
//         // 'openmaptiles': {
//         //     'type': 'vector',
//         //     'tiles': [
//         //         // NOTE: Layers from Stadia Maps do not require an API key for localhost development or most production
//         //         // web deployments. See https://docs.stadiamaps.com/authentication/ for details.
//         //         'https://d1zqyi8v6vm8p9.cloudfront.net/planet/{z}/{x}/{y}.mvt'
//         //     ],
//         //     'maxzoom': 14
//         // },
//         'raster-tiles': {
//             'type': 'raster',
//             'tiles': [
//                 // NOTE: Layers from Stadia Maps do not require an API key for localhost development or most production
//                 // web deployments. See https://docs.stadiamaps.com/authentication/ for details.
//                 'http://a.tile.openstreetmap.org/{z}/{x}/{y}.png'
//             ],
//             'tileSize': 256,
//             'attribution':
//                 'Map tiles by <a target="_blank" href="http://stamen.com">Stamen Design</a>; Hosting by <a href="https://stadiamaps.com/" target="_blank">Stadia Maps</a>. Data &copy; <a href="https://www.openstreetmap.org/about" target="_blank">OpenStreetMap</a> contributors'
//         },
//     },
//     'layers': [
//         // ...positron_layers,
//         // {
//         //     'id': 'vec-tiles',
//         //     'type': 'line',
//         //     'source': 'openmaptiles',
//         //     'source-layer': 'railway',
//         //     'minzoom': 0,
//         //     'maxzoom': 22
//         // },
//         {
//                 'id': 'vec-tiles',
//                 'type': 'raster',
//                 'source': 'raster-tiles',
//                 'source-layer': 'railway',
//                 'minzoom': 0,
//                 'maxzoom': 22
//             },
//     ]
// },
center: [4.284754, 52.111513],
    zoom: 8
});

function filterBy(month, ignore=false) {
    console.log(month, ignore)
    if (!ignore) {
    const filters_until = ["all", ['>=', 'until_year', month], ['<=', 'from_year', month]];
    map.setFilter('places', filters_until);
    } else {
        map.setFilter('places');
    }
    document.getElementById('filter-year').textContent = month;
}

function updateLegend() {
    console.log('Updating legend')
    const features = map.queryRenderedFeatures({ layers: ['places'] });
        let legendMap = {}
    res = features.map(element => {
        legendMap[element['properties']['Operator']] = element['properties']['color']
    });

    console.log(legendMap);

    document.getElementById('layers').innerHTML = Object.keys(legendMap).map(key => {
        return `<div><span style="width:0.1em;background-color: ${stylingMap['operators'][key]};">&nbsp;&nbsp;&nbsp;</span> ${key}</div>`
    }).join('')
}



// Load data
// On load map
map.on('load', async () => {
    // const tramData = await fetch('data/geo/mygeodata_merged.geojson')
    const tramData = await fetch('data/processed/segments.geojson')
        .then(function (response) {
            return response.json();
        }).catch(function (err) {
            console.log('error: ' + err);
        });
        const lijnData = await fetch('data/info/zh.json')
        .then(function (response) {
            return response.json();
        }).catch(function (err) {
            console.log('error: ' + err);
        });
    console.log(lijnData)

    let legendMap = {}
    res = tramData['features'].map(element => {

        // Bezier curves
        // if(element['geometry']['type'] == 'LineString') {
        //     // var bez = turf.bezierSpline(turf.lineString(element['geometry']['coordinates']))['geometry']['coordinates']
        //     // console.log(bez)
        //     element['geometry']['coordinates'] = turf.bezierSpline(turf.lineString(element['geometry']['coordinates']), sharpness=0.99)['geometry']['coordinates']
        // }
        // console.log(element['geometry'])
        element['properties']['color'] = stylingMap['operators'][element['properties']['Operator']]
        legendMap[element['properties']['Operator']] = element['properties']['color']
    });

    console.log(legendMap);

    document.getElementById('layers').innerHTML = Object.keys(legendMap).map(key => {
        return `<div><span style="width:0.1em;background-color: ${stylingMap['operators'][key]};">&nbsp;&nbsp;&nbsp;</span> ${key}</div>`
    }).join('')

    map.addSource('uploaded-source', {
        'type': 'geojson',
        'data': tramData
    });

    map.addLayer({
        'id': 'places',
        'type': 'line',
        'source': 'uploaded-source',
        'layout': {
            'line-join': 'round',
            'line-cap': 'round'
        },
        'paint': {
            // 'line-color': '#888',
            'line-color': ['get', 'color'],
            'line-width': 8,
            // Dashed lines
            // 'line-dasharray': [1, 2],
        }
    });


    // Create popup handlers
    // Create a popup, but don't add it to the map yet.
    const popup = new maplibregl.Popup({
        closeButton: false,
        closeOnClick: false
    });

    map.on('move', updateLegend);

    map.on('mouseenter', 'places', (e) => {
        // Change the cursor style as a UI indicator.
        map.getCanvas().style.cursor = 'pointer';

        // let coordinates = e.features[0].geometry.coordinates.slice();
        let coordinates = e.lngLat;
        const description = e.features[0].properties.description;
        const feature_name = e.features[0].properties.Name;
        const operator = e.features[0].properties.Operator;

        // Ensure that if the map is zoomed out such that multiple
        // copies of the feature are visible, the popup appears
        // over the copy being pointed to.
        console.log(e)
        while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
            coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
        }

        // Populate the popup and set its coordinates
        // based on the feature found.
        // Hacky way to get the coordinates
        if (coordinates[0] instanceof Array) {
            coordinates = coordinates[0]
            if (coordinates[0] instanceof Array) {
                coordinates = coordinates[0]
            }
        }
        const line_key = e.features[0].properties.line_key;
        console.log(e.features[0].properties)
        // console.log(lijnData[line_key])
        if (e.features[0].properties.line_key) {  
            document.getElementById('sidebar-title').innerHTML = ''
            document.getElementById('sidebar-data').innerHTML = format_info_box(e.features[0].properties)
        } else {
            document.getElementById('sidebar-title').innerHTML = `${operator} - ${feature_name}`
            document.getElementById('sidebar-data').innerHTML = description
        }

        popup.setLngLat(coordinates).setHTML(`${operator} - ${feature_name}`).addTo(map);
    });

    map.on('mouseleave', 'places', () => {
        map.getCanvas().style.cursor = '';
        popup.remove();
    });
    document
        .getElementById('slider')
        .addEventListener('input', (e) => {
            const month = parseInt(e.target.value, 10);
            const ignore = document.getElementById('ignore-years').checked
            filterBy(month, ignore);
            updateLegend()
        });

    document
    .getElementById('play-events')
    .addEventListener('click', (e) => {
        // Set the value of the button to Playing
        e.target.textContent = 'Playing';
        // Disable the button
        e.target.disabled = true;

    

        // Print min value and max value of the slider
        const min = parseInt(document.getElementById('slider').min, 10);
        const max = parseInt(document.getElementById('slider').max, 10);
        let i = min;
        // Get the speed from the input field speed
        let speed = parseInt(document.getElementById('speed').value, 10);
        const ignore = document.getElementById('ignore-years').checked
        const interval = setInterval(() => {
            if (i > max) {
                clearInterval(interval);
            } else {
                document.getElementById('slider').value = i;
                filterBy(i, ignore);
                i += 1;
            }
            updateLegend()
        }, speed);

        // Reset the value of the button to Play
        setTimeout(() => {
            e.target.textContent = 'Play';
            e.target.disabled = false
        }, speed * (max - min + 1));
    });

    document.getElementById('ignore-years').addEventListener('change', (e) => {
        const ignore = e.target.checked
        const slider_val = parseInt(document.getElementById('slider').value, 10);
        filterBy(slider_val, ignore);
        // Check if map is finished updating
        map.once('idle', () => {
            updateLegend()
        });
    });
    // Enable to show the sidebar information panel
    // toggleSidebar('left');
});
})();