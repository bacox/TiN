import pandas as pd
import geopandas as gpd
from pathlib import Path
# import locale
# locale.setlocale(locale.LC_ALL, "nl_NL")


def read_data(file_path):
    return pd.read_csv(file_path)


def aux_get_resource_data() -> gpd.GeoDataFrame:
    # Load segments_meta.csv as pd dataframe
    segments_meta = read_data('data/raw/segments_meta.csv')

    # Load all geojson files in raw/segments as geojson dataframes
    def load_geojson_files(directory):
        geojson_files = Path(directory).glob("*.geojson")
        geojson_dataframes = []
        for file in geojson_files:
            geojson_dataframes.append(gpd.read_file(file))
        return geojson_dataframes

    # Load all geojson files in raw/segments as geojson dataframes
    geojson_dataframes = load_geojson_files('data/raw/segments')

    # Merge into single dataframe
    df = gpd.GeoDataFrame(pd.concat(geojson_dataframes, ignore_index=True))

    # Combine with segments_meta based on the line_key
    df_merged = df.merge(segments_meta, on=['line_key'])

    df_merged['from_year'] = pd.to_datetime(df_merged['From']).dt.year
    df_merged['until_year'] = pd.to_datetime(df_merged['Until']).dt.year

    return df_merged

def bundle_resources():
    #Load data
    df_merged = aux_get_resource_data()
    # For each row calculate the length of the segment in meters
    df_merged['length'] = df_merged['geometry'].to_crs('EPSG:3857').length
    

    out_path = Path('data/processed/segments.geojson')
    # Make sure the output directory exists before writing the file
    out_path.parent.mkdir(parents=True, exist_ok=True)
    df_merged.to_file(out_path, driver='GeoJSON')

def bundle_individual_resources():
    #Load data
    df_merged = aux_get_resource_data()

    # Split the dataframe based on the line_key
    for line_key in df_merged['line_key'].unique():
        df_line = df_merged[df_merged['line_key'] == line_key]
        out_path = Path('data/processed/segments/{}.geojson'.format(line_key))
        # Make sure the output directory exists before writing the file
        out_path.parent.mkdir(parents=True, exist_ok=True)
        df_line.to_file(out_path, driver='GeoJSON')


if __name__ == '__main__':
    bundle_resources()
    # bundle_individual_resources()
